##Description

Description about the project. 
- History
- Inspiration
- Why this project, etc!

##How to use instruction
This crawler starts with a target url, fetches the web-page of that url and parser all the links  of that page and stores it in a repo. Next it uses the url from the  repo  and repears the same process.This process goes on till a respective number of links are fetched or if it reaches its depth.

In Order to use:
```
$ ./crawler -d5 <url>
here -d5 means the depth of crawler
```

##Where to get help
- Create an issue in case you found a bug

##Contribution guidelines
- How to contribute
- Styleguide
- Wanted features.
- Links to open issues

##Contributor list
- Vinit Kumar(@vinitcool76)




